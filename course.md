# GitLab für Projektmanagement

Diese kurze Einführung in das Projektmanagement mit GitLab richtet sich in erster Linie an interessierte Personen, die nicht selbst Software entwickeln und ihre Projekte mit GitLab managen oder in Projekten mitarbeiten möchten. In dieser Einführung wird daher nur die grobe Struktur von GitLab Projekten und die Verwaltung von Aufgaben tehmatisiert und nicht näher auf die Möglichkeiten der Versionsverwaltung von Dateien eingegangen.

## Was ist GitLab?

GitLab ist eine Webanwendung für die  Versionsverwaltung von - ursprünglich - Software Projekten auf der Basis von Git. [Wikipedia](https://de.wikipedia.org/wiki/GitLab) In den letzten Jahren sind neben der Versionsverwaltung zahlreiche weitere Features für die Verwaltung von Projekten hinzugekommen, wie z.B. die Verwaltung von Aufgaben, die Möglichkeit der Dokumentation von Projekten in einem Wiki oder Mechanismen zu Automatisierung von Prozessen.

Als Ergänzung zu dieser Einführung sind zwei Quellen als wesentliche Referenz zu nennen

* Das online frei verfügbare Buch zur Versionskontrolle Git [Pro Git](https://git-scm.com/book/de/v2)
* Die (Anwender-)Dokumentation von [GitLab](https://docs.gitlab.com/ee/user/)

Einige der Funktionen von GitLab, wie z.B. die Verwendung von Epics oder Roadmaps, stehen nur in der Premium oder Ultimate Version von GitLab zur Verfügung. Im Folgenden werden daher zunächst nur die Features näher erläutert, die in allen Versionen verfügbar sind.

## Erste Schritte mit GitLab

Bevor wir auf die Funktionen zur Verwaltung von Aufgaben in den Projekten kommen, steigen wir zunächst mit ein paar allgemeinen Punkten zur Orientierung in GitLab ein.

*TODO: Ausführung der folgenden Punkte im Detail*

* Registrieren/Anmelden
  - die Meldung "SSH-Key" bezieht sich nur auf die Arbeit und Synchronisation mit lokal auf dem eigenen Rechner  gespeicherten Projekten und kann an dieser Stelle erstmal ignoriert bzw. geschlossen werden.
* Der eigene Workspace
* Grundlagen zu (ggf. mit Folien)
    - Organisation von Projekten und Gruppen von Projekten
    - Rollen, Rechte und die Sichtbarkeit von Projekten
    - **Übung:**
        * eine Organisationsstruktur anlegen
        * Personen einladen und Rollen zuweisen

## Die Elemente des Projektmanagements in GitLab

###  Issues (Themen)

Nutzen Sie Themen, um gemeinsam an Ideen zu arbeiten, Probleme zu lösen und die Arbeit zu planen. Tauschen Sie Vorschläge mit Ihrem Team und mit externen Mitarbeitern aus und diskutieren Sie sie.

Sie können Issues für viele Zwecke verwenden, angepasst an Ihre Bedürfnisse und Arbeitsabläufe.

* Diskutieren Sie die Umsetzung einer Idee.
* Verfolgen Sie Aufgaben und den Arbeitsstatus.
* Nehmen Sie Feature-Vorschläge, Fragen, Support-Anfragen oder Fehlerberichte an.
* Ausarbeitung von Code-Implementierungen.

*TODO: hier noch Details zu folgenden Punkten zu Issues ausführen*
* Metadaten
* Operationen
* Quick Actions
* Description Templates
* **Übung:** Aufgaben organisieren und Workflow durchlaufen
    - beschreiben, kommentieren
    - zuweisen
    - Status ändern


GitLab Dokumentation zu [Themen](https://docs.gitlab.com/ee/user/project/issues/)

### Tasks (Aufgaben)

Verwenden Sie Aufgaben, um die Schritte zu verfolgen, die für den Abschluss des Problems erforderlich sind.

Bei der Planung eines Problems benötigen Sie eine Möglichkeit, die technischen Anforderungen oder die für den Abschluss des Problems erforderlichen Schritte zu erfassen und aufzuschlüsseln. Ein Problem mit zugehörigen Aufgaben ist besser definiert, so dass Sie eine genauere Problemgewichtung und Abschlusskriterien angeben können.

*TODO: die Verwendung von Tasks kurz erläutern*

GitLab Dokumentation zu [Aufgaben](https://docs.gitlab.com/ee/user/tasks.html)

### Labels

*TODO: die Verwenung von Labels and Issues und Boards erläutern*

GitLab Dokumentation zu [Labels](https://docs.gitlab.com/ee/user/project/labels.html)

### Meilensteine

Mit Meilensteinen in GitLab können Sie Issues verfolgen, um ein breiteres Ziel innerhalb eines bestimmten Zeitraums zu erreichen.

Mit Meilensteinen können Sie Issues in einer zusammenhängenden Gruppe organisieren, mit einem optionalen Startdatum und einem optionalen Fälligkeitsdatum

GitLab Dokumentation zu [Meilensteinen](https://docs.gitlab.com/ee/user/project/milestones/)

*TODO: Issues den Meilensteinen zuordnung und Überblick verschaffen über den Fortschritt des Projekts*

## Issue Boards

(hier zuächst nur Kanban - Kurzbeschreibung)

* Struktur
* Kriterien
* Details
* **Übung:** erstellen und anpassen eines Boards

## Kurzer Einstieg in die Dateiverwaltung

Das Repository in einem GitLab-Projekt umfasst die Dateiverwaltung des Projekts. Besonders geeignet sind reine Textdateien, wie z.B. Quelltexte von Software-Projekten, HTML, XML oder JSON oder auch Textdateien in Markdown, LaTeX oder anderen Auszeichnungssprachen. Andere Formate, wie z.B. Office-Dokumente, PDFs oder Bilder können hier ebenfalls gespeichert werden, allerdings kann GitLab diese nicht so effizient versionieren.

Dateien können allgemein über die Weboberfläche hochgeladen werden und Textdateien können darüber hinaus auch in der Weboberfläche erstellt und bearbeitet werden.  

* Textdateien erstellen oder hochladen
* Markdown als Auszeichnungssprache für Texte, Aufgaben und Kommentare (ggf. eigener Punkt weiter oben?)
