# Gitlab für Projektmanagement

Dieser Kurs bietet einen kurzen Einstieg in das Projektmanagement mit GitLab.

## Formate
* [HTML](https://tibhannover.gitlab.io/tutorials/gitlab-for-project-management/index.html)
* [PDF](https://tibhannover.gitlab.io/tutorials/gitlab-for-project-management/course.pdf)
* [EPUB](https://tibhannover.gitlab.io/tutorials/gitlab-for-project-management/course.epub)
* [LiaScript](https://liascript.github.io/course/?https://tibhannover.gitlab.io/tutorials/gitlab-for-project-management/course.md)
